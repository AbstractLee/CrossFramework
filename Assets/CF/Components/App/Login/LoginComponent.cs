﻿using System;
using CF.CFLog;
using CF.Core;
using CF.Event;

namespace CF.Components.App.Login
{
    /// <summary>
    /// 登陆模块
    /// </summary>
    public class LoginComponent : BaseComponent
    {
        /// <summary>
        /// 登陆完成回调
        /// </summary>
        public Action OnLoginDone;
        
        // private readonly GameEvent LoginEvent = new GameEvent();
        
        public override void OnInit()
        {
            Log.Info("Login Component OnInit");
            // Thread.Sleep(5000);
            // Log.Info("Login Component OnInitAsync5");
            // var listener = LoginEvent.AddListener<string>((str) =>
            // {
            //     
            // });
        }

        public override void OnReady()
        {
            Log.Info("Login Component OnReady");
            // 暂时模拟登陆完成
            OnLoginDone?.Invoke();
            // LoginEvent.SendMessage("hello");
        }

        public override void OnDestroy()
        {
            Log.Info("Login Component OnDestroy");
        }
    }
}