﻿namespace CF.Pool
{
    /// <summary>
    /// 对象池配置
    /// </summary>
    public class PoolConfig
    {
        /// <summary>
        /// 对象池最大数量
        /// </summary>
        public int MaxObjectCount;

        /// <summary>
        /// 是否在卸载场景的时候清理对象池
        /// </summary>
        public bool IsClearOnSceneUnloaded;
    }
}