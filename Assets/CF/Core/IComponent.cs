﻿namespace CF.Core
{
    public interface IComponent
    {
        /// <summary>
        /// Initialize
        /// </summary>
        void Initialize ();

        /// <summary>
        /// Initialized 
        /// </summary>
        void Initialized ();
    }
}