﻿namespace CF.Core
{
    /// <summary>
    /// Base Component
    /// </summary>
    public abstract class BaseComponent
    {
        public abstract void OnInit();

        public abstract void OnReady();

        public abstract void OnDestroy();
    }
}