﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CF.CFLog;

namespace CF.Core
{
    /// <summary>
    /// Game Core
    /// </summary>
    public class Game : IEntry
    {
        /// <summary>
        /// Game components
        /// </summary>
        private static List<BaseComponent> components;
        
        /// <summary>
        /// Init tasks
        /// </summary>
        private static List<Task> tasks;

        public Task OnEntryInit()
        {
            components = new List<BaseComponent>();
            tasks = new List<Task>();
            Log.Info("Game start init...");
            // AddComponent(new LoginComponent());
            return Task.WhenAll(tasks);
        }

        public void OnEntryReady()
        {
            Log.Info("Game start ready...");
            components.ForEach(component =>
            {
                Task.Run(component.OnReady);
            });
        }

        public void OnEntryDestroy()
        {
            Log.Info("Game start destroy...");
            components.ForEach(component =>
            {
                component.OnDestroy();
            });
        }

        private void AddComponent(BaseComponent baseComponent)
        {
            components.Add(baseComponent);
            tasks.Add(Task.Run(baseComponent.OnInit));
        }
    }
}