﻿using System.Threading.Tasks;

namespace CF.Core
{
    public interface IEntry
    {
        Task  OnEntryInit();
        void OnEntryReady();
        void OnEntryDestroy();
    }
}