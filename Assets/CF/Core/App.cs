﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CF.CFLog;
using CF.Components.App.Login;

namespace CF.Core
{
    public class App : IEntry
    {
        /// <summary>
        /// App components
        /// </summary>
        private static List<BaseComponent> components;

        /// <summary>
        /// Init tasks
        /// </summary>
        private static List<Task> tasks;
        
        /// <summary>
        /// Login Component
        /// </summary>
        public static LoginComponent LoginComponent;
        
        public Task OnEntryInit()
        {
            components = new List<BaseComponent>();
            tasks = new List<Task>();
            Log.Info("App start init...");
            return Task.Run(() =>
            {
                AddComponent(LoginComponent = new LoginComponent());
            });
        }

        public void OnEntryReady()
        {
            Log.Info("App start ready...");
            components.ForEach(component =>
            {
                Task.Factory.StartNew(component.OnReady);
            });
        }

        public void OnEntryDestroy()
        {
            Log.Info("App start destroy...");
            components.ForEach(component =>
            {
                component.OnDestroy();
            });
        }

        private void AddComponent(BaseComponent baseComponent)
        {
            components.Add(baseComponent);
            baseComponent.OnInit();
            // tasks.Add(Task.Factory.StartNew(baseComponent.OnInitAsync));
        }
    }
}