﻿using CF.CFLog;
using UnityEngine;

namespace CF.Core
{
    /// <summary>
    /// CrossFramework Entry
    /// </summary>
    public class CFEntry : MonoBehaviour
    {
        private App app;
        private Game game;

        private async void Start()
        {
            app = new App();
            // 异步Init完成表示App模块已经初始化完成
            await app.OnEntryInit();
            Log.Info("App Init Done.");
            App.LoginComponent.OnLoginDone += OnAppLoginDone;
            // Init完成后同步执行ready，不需要等待ready执行完成
            app.OnEntryReady();
        }

        private void OnDestroy()
        {
            game?.OnEntryDestroy();
            if (App.LoginComponent.OnLoginDone != null)
            {
                App.LoginComponent.OnLoginDone -= OnAppLoginDone;
            }
            app.OnEntryDestroy();
        }

        /// <summary>
        /// App 登陆成功
        /// </summary>
        public async void OnAppLoginDone()
        {
            game = new Game();
            await game.OnEntryInit();
            Log.Info("Game Init Done.");
            game.OnEntryReady();
        }
    }
}