﻿using System;
using UnityEngine;

namespace CF.CFLog
{
    [Flags]
    public enum LogMode
    {
        Open = 1 << 0,
        Close = 1 << 1,
        OnlyError = 1 << 2,
    }

    /// <summary>
    /// CF Log
    /// </summary>
    public static class Log
    {
        static Log()
        {
            Mode = LogMode.Open;
        }

        /// <summary>
        /// Log Mode
        /// </summary>
        public static LogMode Mode { get; private set; }

        /// <summary>
        /// Set log type
        /// </summary>
        /// <param name="logMode"></param>
        public static void SetLogType(LogMode logMode)
        {
            Mode = logMode;
        }

        /// <summary>
        /// Log info
        /// </summary>
        /// <param name="message">Message</param>
        public static void Info(object message)
        {
            if (Mode == LogMode.Open)
            {
                var method = new System.Diagnostics.StackTrace().GetFrame(1).GetMethod();
                if (method.ReflectedType != null)
                {
                    Debug.LogFormat($"<color=green>[{method.ReflectedType.FullName}]</color>: {message}");
                }
                else
                {
                    Debug.Log(message);
                }
            }
        }

        /// <summary>
        /// Log warning
        /// </summary>
        /// <param name="message">Message</param>
        public static void Warning(object message)
        {
            if (Mode == LogMode.Open)
            {
                var method = new System.Diagnostics.StackTrace().GetFrame(1).GetMethod();
                if (method.ReflectedType != null)
                {
                    Debug.LogWarningFormat($"<color=yellow>[{method.ReflectedType.FullName}]</color>: {message}");
                }
                else
                {
                    Debug.LogWarning(message);
                }
            }
        }

        /// <summary>
        /// Log error
        /// </summary>
        /// <param name="message">Message</param>
        public static void Error(object message)
        {
            if (Mode == LogMode.Open || Mode == LogMode.OnlyError)
            {
                var method = new System.Diagnostics.StackTrace().GetFrame(1).GetMethod();
                if (method.ReflectedType != null)
                {
                    Debug.LogErrorFormat($"<color=red>[{method.ReflectedType.FullName}]</color>: {message}");
                }
                else
                {
                    Debug.LogError(message);
                }
            }
        }
    }
}