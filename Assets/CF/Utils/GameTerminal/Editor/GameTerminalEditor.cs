﻿using CF.Core;
using UnityEditor;

namespace CF.Utils.GameTerminal.Editor
{
    public class GameTerminalEditor : UnityEditor.Editor
    {
        [UnityEditor.Callbacks.DidReloadScripts]
        private static void OnScriptsReloaded()
        {
            GameTerminal.GetAllAttributeMethods();
        }

        [MenuItem("Tools/GameTerminalTest-Static")]
        private static void TestCmd()
        {
            GameTerminal.ExecuteTerminalCmd("CF.Utils.GameTerminal.GameTerminalTest.Test");
        }
    }
}
