﻿using System;

namespace CF.Event
{
    public class GameEvent
    {
        public Delegate AddListener(Action listenter)
        {
            EventManager.AddListener(this, listenter);
            return listenter;
        }

        public Delegate AddListener<T1>(Action<T1> listenter)
        {
            EventManager.AddListener(this, listenter);
            return listenter;
        }

        public Delegate AddListener<T1, T2>(Action<T1, T2> listenter)
        {
            EventManager.AddListener(this, listenter);
            return listenter;
        }

        public Delegate AddListener<T1, T2, T3>(Action<T1, T2, T3> listenter)
        {
            EventManager.AddListener(this, listenter);
            return listenter;
        }

        public Delegate AddListener<T1, T2, T3, T4>(Action<T1, T2, T3, T4> listenter)
        {
            EventManager.AddListener(this, listenter);
            return listenter;
        }

        public Delegate AddListener<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> listenter)
        {
            EventManager.AddListener(this, listenter);
            return listenter;
        }

        public void RemoveListener(Action listenter)
        {
            EventManager.RemoveListener(this, listenter);
        }

        public void RemoveListener<T1>(Action<T1> listenter)
        {
            EventManager.RemoveListener(this, listenter);
        }

        public void RemoveListener<T1, T2>(Action<T1, T2> listenter)
        {
            EventManager.RemoveListener(this, listenter);
        }

        public void RemoveListener<T1, T2, T3>(Action<T1, T2, T3> listenter)
        {
            EventManager.RemoveListener(this, listenter);
        }

        public void RemoveListener<T1, T2, T3, T4>(Action<T1, T2, T3, T4> listenter)
        {
            EventManager.RemoveListener(this, listenter);
        }

        public void RemoveListener<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> listenter)
        {
            EventManager.RemoveListener(this, listenter);
        }

        public void SendMessage()
        {
            EventManager.SendMessage(this);
        }

        public void SendMessage<T1>(T1 arg1)
        {
            EventManager.SendMessage(this, arg1);
        }

        public void SendMessage<T1, T2>(T1 arg1, T2 arg2)
        {
            EventManager.SendMessage(this, arg1, arg2);
        }

        public void SendMessage<T1, T2, T3>(T1 arg1, T2 arg2, T3 arg3)
        {
            EventManager.SendMessage(this, arg1, arg2, arg3);
        }

        public void SendMessage<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            EventManager.SendMessage(this, arg1, arg2, arg3, arg4);
        }

        public void SendMessage<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            EventManager.SendMessage(this, arg1, arg2, arg3, arg4, arg5);
        }

        public void Remove(Delegate dDelegate)
        {
            EventManager.TryRemoveListener(this, dDelegate);
        }

        public void ClearAll()
        {
            EventManager.Clear(this);
        }
    }
}