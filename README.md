# CrossFramework

​	三亿游戏程序员的梦想！CF框架！

##  目录结构

* CF CrossFramework框架目录
  * Components CF组件目录
  * Core 框架核心目录
  * Editor 框架编辑器目录
* Game GamePlay目录
  * _Scenes 游戏场景目录
  * FX 游戏特效目录
  * ...
* Plugins 自定义插件目录
* Third_Party 第三方插件目录
* UI UI逻辑目录
* ZTest 测试目录

